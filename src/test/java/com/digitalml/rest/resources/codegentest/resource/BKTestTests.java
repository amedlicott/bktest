package com.digitalml.rest.resources.codegentest.resource;

import java.security.Principal;

import org.apache.commons.collections.CollectionUtils;

import org.junit.Assert;
import org.junit.Test;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

public class BKTestTests {

	@Test
	public void testResourceInitialisation() {
		BKTestResource resource = new BKTestResource();
		Assert.assertNotNull(resource);
	}

	@Test
	public void testOperationNewOperation1NoSecurity() {
		BKTestResource resource = new BKTestResource();
		resource.setSecurityContext(null);

		Response response = resource._NewOperation_1(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationNewOperation1NotAuthorised() {
		BKTestResource resource = new BKTestResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource._NewOperation_1(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationNewOperation1Authorised() {
		BKTestResource resource = new BKTestResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource._NewOperation_1(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(404, response.getStatus());
	}

	private SecurityContext unautheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return false;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

}